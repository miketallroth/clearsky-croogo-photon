/**
 * Albums
 *
 * for AlbumsController
 * (copied from default nodes.js)
 */
var Albums = {};

/**
 * functions to execute when document is ready
 *
 * only for AlbumsController
 *
 * @return void
 */
Albums.documentReady = function() {
	Albums.filter();
	Albums.addMeta();
	Albums.removeMeta();
}

/**
 * Submits form for filtering Albums
 *
 * @return void
 */
Albums.filter = function() {
	$('.albums div.actions a.filter').click(function() {
		$('.albums div.filter').slideToggle();
		return false;
	});

	$('#FilterAddForm div.submit input').click(function() {
		$('#FilterAddForm').submit();
		return false;
	});

	$('#FilterAdminIndexForm').submit(function() {
		var filter = '';
		var q='';

		// type
		if ($('#FilterType').val() != '') {
			filter += 'type:' + $('#FilterType').val() + ';';
		}

		// status
		if ($('#FilterStatus').val() != '') {
			filter += 'status:' + $('#FilterStatus').val() + ';';
		}

		// promoted
		if ($('#FilterPromote').val() != '') {
			filter += 'promote:' + $('#FilterPromote').val() + ';';
		}

		//query string
		if($('#FilterQ').val() != '') {
			q=$('#FilterQ').val();
		}
		var loadUrl = Croogo.basePath + 'admin/albums/index/';
		if (filter != '') {
			loadUrl += 'filter:' + filter;
		}
		if (q != '') {
			if (filter == '') {
				loadUrl +='q:'+q;
			} else {
				loadUrl +='/q:'+q;
			}
		}

		window.location = loadUrl;
		return false;
	});
}

/**
 * add meta field
 *
 * @return void
 */
Albums.addMeta = function() {
	$('a.add-meta').click(function(e) {
		var aAddMeta = $(this);
		$.get(aAddMeta.attr('href'), function(data) {
			aAddMeta.parent().find('.clear:first').before(data);
			$('div.meta a.remove-meta').unbind();
			Albums.removeMeta();
		});
		e.preventDefault();
	});
}

/**
 * remove meta field
 *
 * @return void
 */
Albums.removeMeta = function() {
	$('div.meta a.remove-meta').click(function(e) {
		var aRemoveMeta = $(this);
		if (aRemoveMeta.attr('rel') != '') {
			$.getJSON(aRemoveMeta.attr('href') + '.json', function(data) {
				if (data.success) {
					aRemoveMeta.parents('.meta').remove();
				} else {
					// error
				}
			});
		} else {
			aRemoveMeta.parents('.meta').remove();
		}
		e.preventDefault();
	});
}

/**
 * Create slugs based on title field
 *
 * @return void
 */
Albums.slug = function() {
	$("#AlbumTitle").slug({
		slug:'slug',
		hide: false
	});
}

Albums.confirmProcess = function(confirmMessage) {
	var action = $('#AlbumAction :selected');
	if (action.val() == '') {
		confirmMessage = 'Please select an action';
	}
	if (confirmMessage == undefined) {
		confirmMessage = 'Are you sure?';
	} else {
		confirmMessage = confirmMessage.replace(/\%s/, action.text());
	}
	if (confirm(confirmMessage)) {
		action.get(0).form.submit();
	}
	return false;
}

/**
 * document ready
 *
 * @return void
 */
$(document).ready(function() {
	if (Croogo.params.controller == 'albums') {
		Albums.documentReady();
		if (Croogo.params.action == 'admin_add') {
			Albums.slug();
		}
	}
});
