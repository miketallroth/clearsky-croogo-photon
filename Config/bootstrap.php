<?php
/**
 * Routes
 */
	Croogo::hookRoutes('Photon');
/**
 * Behavior
 */
   	Croogo::hookBehavior('Node', 'Photon.RelatedAlbum', array(
	'relationship' => array(
		'hasOne' => array(
			'Album' => array(
				'className' => 'Photon.Album',
				'foreignKey' => 'node_id',
				),
			),
		),
	));
/**
 * Component
 */
	Croogo::hookComponent('Nodes', 'Photon.NodeAlbum');
/**
 * Helper
 */
    Croogo::hookHelper('Nodes', 'Photon.Gallery');
/**
 * Admin menu (navigation)
 *
 * This plugin's admin_menu element will be rendered in admin panel under Media menu.
 */
    Croogo::hookAdminMenu('Photon');
	CroogoNav::add('media.children.photon', array(
		'title' => __('Photon'),
		'url' => array(
			'plugin' => 'photon',
			'admin' => true,
			'controller' => 'albums',
			'action' => 'index',
		),
		'weight' => 20,
		'children' => array(
			'photon1' => array(
				'title' => 'List Albums',
				'url' => array(
					'plugin' => 'photon',
					'admin' => true,
					'controller' => 'albums',
					'action' => 'index',
				),
			),
			'photon2' => array(
				'title' => 'New Album',
				'url' => array(
					'plugin' => 'photon',
					'admin' => true,
					'controller' => 'albums',
					'action' => 'add',
				),
			),
			'photon3' => array(
				'title' => 'List Photos',
				'url' => array(
					'plugin' => 'photon',
					'admin' => true,
					'controller' => 'photos',
					'action' => 'index',
				),
			),
            'photon4' => array(
                'title' => 'Photon Settings',
                'url' => array(
                    'plugin' => false,
                    'admin' => true,
                    'controller' => 'settings',
                    'action' => 'prefix/Photon',
                ),
            ),
		),
	));

/**
 * Admin tab
 */
 	Croogo::hookAdminTab('Nodes/admin_add', 'Images', 'photon.admin_node_add', array('type'=>array('node','page')));
	Croogo::hookAdminTab('Nodes/admin_edit', 'Images', 'photon.admin_node_edit', array('type'=>array('node','page')));
?>
