<?php
/**
 * Album
 *
 * PHP version 5
 *
 * @category Model
 * @package  Croogo
 * @version  1.3
 * @author   bumuckl <bumuckl@gmail.com> and Edinei L. Cipriani <phpedinei@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.bumuckl.com
 */
class Album extends PhotonAppModel {

	public $name = 'Album';
	
	public $actsAs = array(
	      'Ordered' => array(
	          'field' => 'position',
	          'foreign_key' => false,
	      ),
	);

	public $validate = array(
		'slug' => array(
			'rule' => 'isUnique',
			'message' => 'Slug is already in use.',
		),
	);

	public $hasMany = array(
			'Photo' => array(
				'className' => 'Photon.Photo',
				'foreignKey' => 'album_id',
				'dependent' => true,
				'conditions' => '',
				'fields' => '',
				'order' => 'Photo.position ASC',
				'limit' => '',
				'offset' => '',
				'exclusive' => '',
				'finderQuery' => '',
				'counterQuery' => ''
			),
	);
	
	public $belongsTo = array(
        'Node' => array(
            'className'    => 'Node',
            'foreignKey'    => 'node_id'
        )
    );  

	public function paginateCount($conditions = null, $recursive = 0, $extra = array()) {
		if (!empty($extra) && isset($extra['customCount'])) {
			return $extra['customCount'];
		}
        $count = $this->find('count', array(
			'conditions' => $conditions,
		));
        return $count;
    }

}
?>
