<?php
/**
 * Photos Controller
 *
 * PHP version 5
 *
 * @category Controller
 * @package  Croogo
 * @version  1.3
 * @author   bumuckl <bumuckl@gmail.com> and Edinei L. Cipriani <phpedinei@gmail.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.bumuckl.com
 */
class PhotosController extends PhotonAppController {

	public $name = 'Photos';
	
	public $helpers = array(
		'Layout',
		'Html',
	);
	
	/*
	 * @description Shows an index overview for all photos in any album
	 */
	public function admin_index() {
	    $this->set('title_for_layout', __d('photon','Photos', true));

		$this->Photo->recursive = -1;
		$this->paginate = array(
            'limit' => Configure::read('Photon.album_limit_pagination'),
            'order' => 'Photo.position ASC',
		);
		$this->set('photos', $this->paginate('Photo'));
	}

	/*
	 * @description View a single photo
	 * @param int id
	 * @sets array photo, title_for_layout
	 */
	public function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__d('photon','Invalid photo. Please try again.', true));
			$this->redirect(array('controller' => 'albums', 'action' => 'index'));
		}

		$photo = $this->Photo->find('first', array('conditions' => array('Photo.id' => $id)) );
		
		if (isset($this->params['requested'])) {
			return $photo;
		}

		if (!count($photo)) {
			$this->Session->setFlash(__d('photon','Invalid photo. Please try again.', true));
			$this->redirect(array('controller' => 'albums', 'action' => 'index'));
		}

		$this->set('title_for_layout',__d('photon',"Photo", true) . $photo['Photo']['title']);
		$this->set(compact('photo'));
	}
	
	/*
	 * @description Update title and description of a photo, AJAX powered!
	 * @param int id, string title, string description
	 * @sets JSON
	 */
	public function admin_updateTitleAndDescription($id = null, $title = null, $description = null) {
		set_time_limit ( 360 ) ;

		$this->layout = 'ajax';
		$this->render(false);
		Configure::write('debug', 0);

		$this->Photo->id = $id;
		$this->Photo->saveField('title', $title);
		$this->Photo->saveField('description', $description);
		
		//Clearing the cache
		$db = ConnectionManager::getDataSource('default');
		@unlink(TMP . 'cache' . DS . 'models/cake_model_default_' . $db->config["database"] . '_list');
		@unlink(TMP . 'cache' . DS . 'models/cake_model_default_photos');
		@unlink(TMP . 'cache' . DS . 'models/cake_model_default_albums');
		$db->sources(true);
		
		$result = $this->Photo->findById($id);

		echo json_encode($result);
	}

    public function admin_moveup($id, $step = 1) {
        if( $this->Photo->moveUp($id, $step) ) {
            $this->Session->setFlash(__('Moved up successfully', true), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Could not move up', true), 'default', array('class' => 'error'));
        }

		// redirect to album edit view
		$photo = $this->Photo->find('first', array('conditions' => array('Photo.id' => $id)) );
        $this->redirect(array('controller' => 'albums', 'action' => 'edit', $photo['Photo']['album_id'], '#' => 'album-images'));
    }

    public function admin_movedown($id, $step = 1) {
        if( $this->Photo->moveDown($id, $step) ) {
            $this->Session->setFlash(__('Moved down successfully', true), 'default', array('class' => 'success'));
        } else {
            $this->Session->setFlash(__('Could not move down', true), 'default', array('class' => 'error'));
        }

		// redirect to album edit view
		$photo = $this->Photo->find('first', array('conditions' => array('Photo.id' => $id)) );
        $this->redirect(array('controller' => 'albums', 'action' => 'edit', $photo['Photo']['album_id'], '#' => 'album-images'));
    }

	/*
	 * @description Retrieves a list of all photos in an album,
	 *				typically used as request action (notice no 'set').
	 */
	public function album_photos($album_id = null) {

		if ($album_id == null) {
			return array();
		}

		$this->Photo->recursive = -1;
		$photos = $this->Photo->find('all', array(
			'conditions' => array(
				'Photo.album_id' => $album_id,
			),
			'order' => 'Photo.position ASC',
		));

		return $photos;
	}

}
?>
