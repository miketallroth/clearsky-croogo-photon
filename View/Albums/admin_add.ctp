<?php
	$this->extend('/Common/admin_edit');
	$this->Html->script(array('/photon/js/albums'), false);
?>

<?php echo $this->Form->create('Album');?>
    <fieldset>
    <?php
        echo $this->Form->input('title',array('label' => __('Title', true)));
        echo $this->Form->input('slug', array('class' => 'slug'));
        echo $this->Form->input('description',array('label' => __('Description', true)));
        echo $this->Form->input('status');
    ?>
    </fieldset>
	<div class="buttons">
	<?php
		echo $this->Form->submit(__('Apply'), array('name' => 'apply'));
		echo $this->Form->submit(__('Save'));
		echo $this->Html->link(__('Cancel'), array(
			'action' => 'index',
		), array(
			'class' => 'cancel',
		));
	?>
	</div>
<?php echo $this->Form->end();?>
