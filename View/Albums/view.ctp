<header>
    <hgroup>
        <h1><?php echo __d('photon', 'Album');?>: <?php echo $album['Album']['title']; ?></h1>
        <h3><?php echo $album['Album']['description']; ?></h3>
    </hgroup>
</header>
<?php echo $this->element('gallery'); ?>
<footer>
	<nav id="pagination"><?php echo $this->Html->link(__d('photon','Back', true), '/gallery'); ?></nav>
</footer>
