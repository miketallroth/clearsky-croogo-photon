<?php
	$this->extend('/Common/admin_index');
	$this->Html->script(array('/photon/js/albums'), false);

	$this->start('tabs');
	echo '<li>' . $this->Html->link(__('Create Album'), array('action' => 'add')) . '</li>';
	$this->end();
?>

<?php echo $this->Form->create('Album', array('url' => array('controller' => 'albums', 'action' => 'process'))); ?>
<table cellpadding="0" cellspacing="0">
<?php
    $tableHeaders =  $this->Html->tableHeaders(array(
        '',
        $this->Paginator->sort('id'),
        //__d('photon','Order number', true),
        __('Title', true),
        $this->Paginator->sort('status'),                                            
        __('Actions', true),
    ));
    echo $tableHeaders;

    $rows = array();
    foreach ($albums as $album) {
       	$actions = $this->Html->link(__d('photon','Photos in album', true), array('controller' => 'albums', 'action' => 'edit', $album['Album']['id'], '#' => 'album-images'));

		$actions .= ' ' . $this->Html->link(__('Move up', true), array('controller' => 'albums', 'action' => 'moveup', $album['Album']['id']));
        $actions .= ' ' . $this->Html->link(__('Move down', true), array('controller' => 'albums', 'action' => 'movedown', $album['Album']['id']));

		$actions .= ' ' . $this->Layout->adminRowActions($album['Album']['id']);
        $actions .= ' ' . $this->Html->link(__('Edit', true), array('controller' => 'albums', 'action' => 'edit', $album['Album']['id']) );
        $actions .= ' ' . $this->Layout->processLink(__('Delete', true), '#Album' . $album['Album']['id'] . 'Id', null, __('Are you sure you want to delete this album?', true));

        $rows[] = array(
            $this->Form->checkbox('Album.' . $album['Album']['id'] . '.id'),
            $album['Album']['id'],
            //$album['Album']['position'],
            $album['Album']['title'],
            $this->Layout->status($album['Album']['status']),
            $actions,
        );
    }

    echo $this->Html->tableCells($rows);
    echo $tableHeaders;
?>
</table>

<div class="bulk-actions">
<?php
	echo $this->Form->input('Album.action', array(
		'label' => false,
		'options' => array(
			'delete' => __('Delete'),
		),
		'empty' => true,
	));
	$jsVarName = uniqid('confirmMessage_');
	echo $this->Form->button(__('Submit'), array(
		'type' => 'button',
		'onclick' => sprintf('return Albums.confirmProcess(app.%s)', $jsVarName),
	));
	$this->Js->set($jsVarName, __('%s selected items?'));

	echo $this->Form->end();
?>
</div>
