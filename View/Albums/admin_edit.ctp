<?php
	$this->extend('/Common/admin_edit');
	$this->Html->script(array('/photon/js/albums'), false);
?>

<?php echo $this->Form->create('Album');?>
	<fieldset>
    <div class="tabs">
			<ul>
				<li><a href="#album-basic"><span><?php echo __('Settings'); ?></span></a></li>
				<li><a href="#album-images"><span><?php echo __('Images'); ?></span></a></li>
				<?php echo $this->Layout->adminTabs(); ?>
			</ul>
			
			<div id="album-basic">
		        <?php
					echo $this->Form->input('id');
		            echo $this->Form->input('title',array('label' => __('Title', true)));
		            echo $this->Form->input('slug');
					echo $this->Form->input('description',array('label' => __('Description', true)));
					echo $this->Form->input('status');
		        ?>
		    </div>
		    
		    <div id="album-images">
		    	<?php echo $this->element('admin_node_edit', array('album' => $album) ); ?>
		    </div>
			<?php echo $this->Layout->adminTabs(); ?>
	</div>
	</fieldset>
	<div class="buttons">
	<?php
		echo $this->Form->submit(__('Apply'), array('name' => 'apply'));
		echo $this->Form->submit(__('Save'));
		echo $this->Html->link(__('Cancel'), array(
			'action' => 'index',
		), array(
			'class' => 'cancel',
		));
	?>
	</div>
<?php echo $this->Form->end();?>
