<div class="photos index">
    <h2><?php echo $title_for_layout; ?></h2>

    <table cellpadding="0" cellspacing="0">
    <?php
        $tableHeaders =  $this->Html->tableHeaders(array(
                                            __('Picture', true),
											__('URL', true),
                                            __('Embedding', true),                                          
                                            __('Actions', true),
                                            ));
        echo $tableHeaders;

        $rows = array();
        foreach ($photos as $photo) {
           	$actions = $this->Html->link(__d('photon','Album', true), array('controller' => 'albums', 'action' => 'edit', $photo['Photo']['album_id'], '#' => 'album-images'));
			$actions .= ' ' . $this->Layout->adminRowActions($photo['Photo']['id']);

			$smalltag = $this->Html->image('photos/'.$photo['Photo']['small']);
			$largeurl = $this->Html->url('/img/photos/'.$photo['Photo']['large']);

            $rows[] = array(
                        $smalltag,
                        '<a href="' . $largeurl . '">' . $largeurl . '</a>',
                        'Insert [Image:' . $photo['Photo']['id'] . '] into your node.',
                        $actions,
                      );
        }

        echo $this->Html->tableCells($rows);
        echo $tableHeaders;
    ?>
    </table>
</div>

<div class="paging"><?php echo $this->Paginator->numbers(); ?></div>
<div class="counter"><?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}', true))); ?></div>
