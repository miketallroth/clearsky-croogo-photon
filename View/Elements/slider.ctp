<?php 
    $this->Html->css('nivo-themes/bar/bar', null, array('inline'=>false));
    $this->Html->css('nivo-slider', null, array('inline'=>false));

    $this->Html->script('nivo-slider/jquery.nivo.slider.pack', array(
        'inline' => false,
    ));
    echo $this->Html->scriptBlock("
        $(window).load(function() {
            $('#slider').nivoSlider({
                effect: 'fade',
                animSpeed: 500,
                pauseTime: 5000
            });
        });
    ");

	if(!isset($album)) {
		$album = $this->requestAction(array('plugin' => 'photon', 'controller' => 'albums', 'action' => 'view'), array('pass' => array('slug' => $slug)));
	} 
?>
	
<?php if(!empty($album)): ?>

	<?php if(isset($album['Photo']) && count($album['Photo'])): ?>
		<div id="slider" class="slider<?php echo $slider_id; ?>">
			<?php foreach($album['Photo'] as $photo): ?>
				<a	href="<?php echo $this->Html->url('/img/photos/'. $photo['large']); ?>" title="<?php echo $photo['title']; ?>" class="slider" target="_blank">
					<img src="<?php echo $this->Html->url('/img/photos/'. $photo['large']); ?>" alt="<?php echo $photo['title']; ?>">
				</a>
			<?php endforeach; ?>
		</div>
	<?php else: ?>
		<?php  __d('photon','No photos in the album'); ?>
	<?php endif;?>
	
<?php else: ?>[Slider:<?php echo $slug; ?>]<?php endif; ?>
