<div class="album upload">

	<?php
	$album_id = '';
	if (isset($album['Album']['id'])) {
		$album_id = $album['Album']['id'];
	}
	?>

	<h3><?php echo __d('Photon', 'Pictures for this node:', true); ?></h3>
	<small><?php echo __d('Photon', 'Every node has got its private gallery album. Everytime you upload an image in this tab, it will be appended to this album. To show the whole album in your node, paste the following code at the location of your choice (of course this works in other nodes as well):', true); ?><br/>
	[Slider:<?php echo $album['Album']['slug'];?>]
	</small>

<?php
	if (isset($album['Photo'])) {
		echo '<p><table cellpadding="0" cellspacing="0" id="return">';
		echo '<tr>';
		echo '<th>' . __('Picture') . '</th>';
		echo '<th>' . __('Title, Description & URL') . '</th>';
		echo '<th>' . __('Embedding') . '</th>';
		echo '<th>' . __('Actions') . '</th>';
		echo '</tr>';

		foreach($album['Photo'] as $index => $photo) {
			$smalltag = $this->Html->image('photos/'.$photo['small']);
			$largeurl = $this->Html->url('/img/photos/'.$photo['large']);

			echo '<tr>';
			echo "\n";
			echo '<td>' . $smalltag . '</td>';
			echo "\n";
			echo '<td>';
			echo $this->Form->hidden('Photo.' . $index . '.id');
			echo $this->Form->input('Photo.' . $index . '.title');
			echo $this->Form->input('Photo.' . $index . '.description');
			echo __d('photon','URL: ');
			echo '<a href="' . $largeurl . '">' . $largeurl . '</a>';
			echo '</td>';
			echo "\n";
			echo '<td>Insert [Image:' . $photo['id'] . '] into your node.</td>';
			echo '<td>';
            echo $this->Html->link(__('Move up', true), array('plugin' => 'photon', 'controller' => 'photos', 'action' => 'moveup', $photo['id']));
            echo $this->Html->link(__('Move down', true), array('plugin' => 'photon', 'controller' => 'photos', 'action' => 'movedown', $photo['id']));
			echo '<a href="javascript:;" class="remove" rel="' . $photo['id'] . '">' . __d('Photon','Remove') . '</a>';
			echo '</td>';
			echo "\n";
			echo '</tr>';
			echo "\n";
		}
		echo '</table></p>';
	}
?>




	
	<!--This container will be filled with data received from the Uploader-->
	<p><div id="upload">

	</div></p>
	
</div>



<?php
	echo $this->Html->script('/photon/js/fileuploader', false);
	echo $this->Html->css('/photon/css/fileuploader', false);

	$actionurl = $this->Html->url(array(
		'plugin' => 'photon',
		'controller' => 'albums',
		'action' => 'upload_photo',
		$album_id
	));
	$updateurl = $this->Html->url(array(
		'plugin' => 'photon',
		'controller' => 'photos',
		'action' => 'updateTitleAndDescription'
	));
?>

<script>

function createUploader(){     
       
    var uploader = new qq.FileUploader({
        element: document.getElementById('upload'),
        action: '<?php echo $actionurl; ?>',
		onComplete: function(id, fileName, responseJSON){
			$('.qq-upload-fail').fadeOut(function(){
				$(this).remove();
			});

			var id = responseJSON.Photo.id;
			var small = responseJSON.Photo.small;
			var smallurl = "<?php echo $this->Html->url('/img/photos/'); ?>"+small;
			var large = responseJSON.Photo.large;
			var largeurl = "<?php echo $this->Html->url('/img/photos/'); ?>"+large;
			var moveurl = "<?php echo $this->Html->url(array('plugin'=>'photon','controller'=>'photos')); ?>";

			$('#return').append('<tr>' +
				'<td><img src="' + smallurl + '" alt="" /></td>' +
				'<td>' +
				'<input type="hidden" name="data[Photo]['+id+'][id]" value="'+id+'" id="Photo'+id+'Id"/>' +
				'<div class="input text">' +
				'<label for="Photo'+id+'Title">Title</label>' +
				'<input name="data[Photo]['+id+'][title]" maxlength="45" type="text" id="Photo'+id+'Title" />' + 
				'</div>' +
				'<div class="input textarea">' +
				'<label for="Photo'+id+'Description">Description</label>' +
				'<textarea name="data[Photo]['+id+'][description]" cols="30" rows="6" id="Photo'+id+'Description"></textarea>' +
				'</div>' +
				'<?php echo __d('Photon','URL: '); ?><a href="'+largeurl+'">'+largeurl+'</a>' + 
				'</td>' +
				'<td>Insert [Image:'+id+'] into your node.</td>' +
				'<td>' +
				'<a href="' + moveurl + '/moveup/' + id + '">Move up</a>' +
				'<a href="' + moveurl + '/movedown/' + id + '">Move down</a>' +
				'<a href="javascript:;" class="remove" rel="'+id+'"><?php echo __d('Photon','Remove'); ?></a>' +
				'</td>' +
			'</tr>');
		},
		
		template: '<div class="qq-uploader">' + 
			'<div class="qq-upload-drop-area"><span>' +
			'<?php echo __d('Photon','Drop files here to upload'); ?>' +
			'</span></div>' +
			'<a class="qq-upload-button ui-corner-all" style="background-color:#EEEEEE;float:left;font-weight:bold;margin-right:10px;padding:10px;text-decoration:none;cursor:pointer;">' +
			'<?php echo __d('Photon','Add new photos'); ?></a>' +
			'<ul class="qq-upload-list"></ul>' + 
			'</div>',
		      
		fileTemplate: '<li>' +
			'<span class="qq-upload-file"></span>' +
			'<span class="qq-upload-spinner"></span>' +
			'<span class="qq-upload-size"></span>' +
			'<a class="qq-upload-cancel" href="#">' +
			'<?php echo __d('Photon','cancel'); ?></a>' +
			'</li>'

    });
    
    $('input[name^=title]').live('change', function() {
    	
    	var id = parseInt($(this).attr("name").replace("title", ""));
    	var title = $(this).val();
// TODO - what is this change for???
    	//var description = $('input[name=desc'+id+']').val();
    	var description = $('input[name=data\[Photo\]\['+id+'\]\[description\]]').val();
    	$.ajax({
  			url: '<?php echo $updateurl; ?>/'+id+'/'+title+'/'+description,
  			success: function( data ){
    			$(this).val(data.title);
    			$('input[name=desc'+id+']').val(data.description);
  			}
		});
    	
    });
    
    $('input[name^=desc]').live('change', function() {
    	
    	var id = parseInt($(this).attr("name").replace("desc", ""));
    	var description = $(this).val();
    	var title = $('input[name=title'+id+']').val();
    	$.ajax({
  			url: '<?php echo $updateurl; ?>/'+id+'/'+title+'/'+description,
  			success: function( data ){
    			$(this).val(data.description);
    			$('input[name=title'+id+']').val(data.title);
  			}
		});
    	
    });
            
}

// in your app create uploader as soon as the DOM is ready
// don't wait for the window to load  
$(function(){

	createUploader();
	$('.remove').live('click', function(){
		var obj = $(this);
		$.getJSON('<?php echo $this->Html->url('/admin/photon/albums/delete_photo/');?>'+obj.attr('rel'), function(r) {
            if (r['status'] == 1) {
				obj.parent().parent().fadeOut(function(){
					$(this).remove();
				});
            } else {
                alert(r['msg']);	
            }
		});
	});
	
});

</script>
